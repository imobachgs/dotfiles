all: grains salt
	sudo salt-call --local state.apply

grains:
	echo home: ${HOME} > etc/grains
	echo user: `id -un` >> etc/grains
	echo group: `id -gn` >> etc/grains
	echo ruby: `ruby -e "puts RUBY_VERSION.split('.')[0..1].join('.')"` >> etc/grains

salt:
	test -f /usr/bin/salt-call || sudo zypper --non-interactive in salt
