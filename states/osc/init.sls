{% from "osc/map.jinja" import osc with context %}

osc:
  pkg.installed: []

oscrc:
  file.managed:
    - name: {{ grains.home }}/.config/osc/oscrc
    - source: salt://osc/files/oscrc.jinja
    - user: {{ grains.user }}
    - group: {{ grains.group }}
    - mode: 600
    - makedirs: True
    - template: jinja
    - defaults:
      osc: {{ osc }}

/etc/sudoers.d/osc:
  file.managed:
    - source: salt://osc/files/sudoers.jinja
    - user: root
    - group: root
    - mode: 640
    - template: jinja
    - defaults:
        username: {{ grains.user }}
