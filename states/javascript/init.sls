node_packages:
  pkg.installed:
    - pkgs:
      - nodejs-default
      - typescript

npmrc:
  file.managed:
    - name: {{ grains.home }}/.npmrc
    - source: salt://javascript/npmrc
    - user: {{ grains.user }}
    - group: {{ grains.group }}
    - mode: 640
