ruby:
  pkg.installed:
    - pkgs:
      - libxml2-devel
      - libxslt-devel
      - ruby
      - ruby-devel
      - ruby{{ grains.ruby }}-rubygem-bundler
      - ruby{{ grains.ruby }}-rubygem-byebug

gemrc:
  file.managed:
    - name: {{ grains.home }}/.gemrc
    - source: salt://ruby/gemrc
    - user: {{ grains.user }}
    - group: {{ grains.group }}
    - mode: 640
