flatpak:
  pkg.installed: []

flathub:
  cmd.run:
    - name: "flatpak remote-add --if-not-exists flathub https://flathub.org/repo/flathub.flatpakrepo"
    - require:
      - pkg: flatpak

flatpak_apps:
  cmd.run:
    - name: flatpak install -y com.slack.Slack com.discordapp.Discord
    - require:
      - cmd: flathub

desktop_apps:
  pkg.installed: 
    - pkgs:
      - console
      - nextcloud-desktop

firamono:
  archive.extracted:
    - name: {{ grains.home }}/.local/share/fonts/FiraMono
    - source: https://github.com/ryanoasis/nerd-fonts/releases/download/v2.1.0/FiraMono.zip
    - user: {{ grains.user }}
    - group: {{ grains.group }}
    - source_hash: f4e966bddbbd85826c72b5d6dfcf3c2857095f2e2819784b5babc2a95a544d38
    - enforce_toplevel: False
