base:
  '*':
    - console
    - desktop
    - javascript
    - neovim
    - osc
    - ruby
    - rust
    - tmux
