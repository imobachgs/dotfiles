# Ring the bell if any background window rang a bell
set -g bell-action any

# Default termtype. If the rcfile sets $TERM, that overrides this value.
set -g default-terminal "screen-256color"
set -ga terminal-overrides ",*256col*:Tc"

# Keep your finger on ctrl, or don't
bind-key ^D detach-client

# Create splits and vertical splits
bind-key v split-window -h
bind-key ^V split-window -h
bind-key s split-window
bind-key ^S split-window

# Pane resize in all four directions using vi bindings.
# Can use these raw but I map them to shift-ctrl-<h,j,k,l> in iTerm.
bind-key J resize-pane -D
bind-key K resize-pane -U
bind-key H resize-pane -L
bind-key L resize-pane -R

# Use vi keybindings for tmux commandline input.
# Note that to get command mode you need to hit ESC twice...
set -g status-keys vi

# Use vi keybindings in copy and choice modes
setw -g mode-keys vi

# easily toggle synchronization (mnemonic: e is for echo)
# sends input to all panes in a given window.
bind e setw synchronize-panes on
bind E setw synchronize-panes off

# set first window to index 1 (not 0) to map more to the keyboard layout...
set-option -g base-index 1
set-window-option -g pane-base-index 1

# Screen like binding
unbind C-b
set -g prefix C-a
bind a send-prefix

# No escape time for vi mode
set -sg escape-time 0

# Screen like binding for last window
unbind l
bind C-a last-window

# Bigger history
set -g history-limit 10000

# Local config
#if-shell "[ -f ~/.tmux.conf.user ]" 'source ~/.tmux.conf.user'

# List of plugins
set -g @plugin 'tmux-plugins/tpm'
set -g @plugin 'christoomey/vim-tmux-navigator'

# Initialize TMUX plugin manager (keep this line at the very bottom of tmux.conf)
run '~/.tmux/plugins/tpm/tpm'

source ~/.tmux-theme.conf
