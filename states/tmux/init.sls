tmux:
  pkg.installed:
    - pkgs:
      - tmux
      - tmate

tmux.conf:
  file.managed:
    - name: {{ grains.home }}/.tmux.conf
    - source: salt://tmux/tmux.conf
    - user: {{ grains.user }}
    - group: {{ grains.group }}
    - mode: 640

tmux-theme.conf:
  file.managed:
    - name: {{ grains.home }}/.tmux-theme.conf
    - source: salt://tmux/tmux-theme.conf
    - user: {{ grains.user }}
    - group: {{ grains.group }}
    - mode: 640

tmate.conf:
  file.managed:
    - name: {{ grains.home }}/.tmate.conf
    - source: salt://tmux/tmate.conf
    - user: {{ grains.user }}
    - group: {{ grains.group }}
    - mode: 640

tpm:
  git.latest:
    - name: https://github.com/tmux-plugins/tpm
    - target: {{ grains.home }}/.tmux/plugins/tpm
    - user: {{ grains.user }}

install_tmux_plugins:
  cmd.run:
    - name: {{ grains.home }}/.tmux/plugins/tpm/bin/install_plugins
    - runas: {{ grains.user }}
    - require:
      - pkg: console_packages
      - git: tpm
