fish_add_path bin/
fish_add_path .cargo/env
fish_add_path .local/share/gem/ruby/3.1.0/bin
fish_add_path .local/lib/node_modules/bin
fish_add_path .cargo/bin

set -gx EDITOR nvim

alias vim="nvim"
alias ls="exa"

if status is-interactive
  starship init fish | source
end
