{% set nvim_config = salt['grains.fetch']('home') + '/.config/nvim' %}

neovim:
  pkg.installed:
    - pkgs:
      - neovim
      - git

config:
  file.recurse:
    - name: {{ nvim_config }}
    - source: salt://neovim/nvim
    - user: {{ grains.user }}
    - group: {{ grains.group }}
    - clean: True

packer:
  git.latest:
    - name: https://github.com/wbthomason/packer.nvim
    - target: {{ grains.home }}/.local/share/nvim/site/pack/packer/start/packer.nvim
    - rev: master
    - depth: 1
    - user: {{ grains.user }}
    - require:
      - pkg: neovim
