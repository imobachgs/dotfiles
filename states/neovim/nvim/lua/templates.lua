local utils = require('utils')
utils.create_augroup({
  {
    "BufNewFile",
    "*/yast/*.rb",
    "if &filetype != 'rspec' | 0r ~/.config/nvim/templates/yast-ruby.rb | endif"
  },
  {
    "BufNewFile",
    "*/yast/*.rb",
    "if &filetype == 'rspec' | 0r ~/.config/nvim/templates/yast-rspec.rb | endif"
  }
}, "rspec_autocmds")
