local status_ok, telekasten = pcall(require, "telekasten")
if not status_ok then
  return
end

local home = vim.fn.expand("~/zettelkasten")

-- TODO: telescope-media-files.nvim
-- TODO: https://github.com/iamcco/markdown-preview.nvim
-- TODO: https://github.com/mzlogin/vim-markdown-toc

telekasten.setup({
    home         = home,
    new_note_filename = "uuid",
    tag_notation = "yaml-bare",
    command_palette_theme = "dropdown",
    show_tags_theme = "dropdown",
    template_new_note = home .. '/' .. 'templates/new_note.md',
    template_new_daily = home .. '/' .. 'templates/daily.md',
    template_new_weekly= home .. '/' .. 'templates/weekly.md'
})

-- use the markdown parser
local ft_to_parser = require("nvim-treesitter.parsers").filetype_to_parsername;
ft_to_parser.telekasten = "markdown";

local opts = { noremap=true, silent=true }
vim.api.nvim_create_autocmd("FileType", {
  pattern = "telekasten",
  callback = function()
    vim.api.nvim_buf_set_keymap(0, 'n', 'cc', '<cmd>lua require("telekasten").toggle_todo()<CR>', opts)
  end
});

