local execute = vim.api.nvim_command
local fn = vim.fn
local install_path = fn.stdpath('data')..'/site/pack/packer/start/packer.nvim'

if fn.empty(fn.glob(install_path)) > 0 then
  fn.system({'git', 'clone', 'https://github.com/wbthomason/packer.nvim', install_path})
  execute 'packadd packer.nvim'
end

vim.cmd [[packadd packer.nvim]]

-- Autocommand that reloads neovim whenever you save the plugins.lua file
vim.cmd [[
  augroup packer_user_config
    autocmd!
    autocmd BufWritePost plugins.lua source <afile> | PackerSync
  augroup end
]]

-- Use a protected call so we don't error out on first use
local status_ok, packer = pcall(require, "packer")
if not status_ok then
  return
end

-- Have packer use a popup window
packer.init {
  display = {
    open_fn = function()
      return require("packer.util").float { border = "rounded" }
    end,
  },
}

return packer.startup(function(use)
  use 'wbthomason/packer.nvim'

  -- files
  use {
    'nvim-neo-tree/neo-tree.nvim',
    branch = 'v2.x',
    requires = { 
      'nvim-lua/plenary.nvim',
      'kyazdani42/nvim-web-devicons', -- not strictly required, but recommended
      'MunifTanjim/nui.nvim',
    }
  }

  -- run async tasks
  use {'tpope/vim-dispatch', opt = true, cmd = {'Dispatch', 'Make', 'Focus', 'Start'}}

  -- extras
  use 'tpope/vim-surround'
  use 'christoomey/vim-tmux-navigator'
  use {
    'AckslD/nvim-whichkey-setup.lua',
    requires = {'liuchengxu/vim-which-key'},
  }
  use 'tpope/vim-commentary'
  use 'echasnovski/mini.nvim'
  use 'lambdalisue/suda.vim'
  use 'akinsho/toggleterm.nvim'
  use {
    'phaazon/hop.nvim',
    as = 'hop',
    config = function()
      require'hop'.setup()
    end
  }
  use {
    'lukas-reineke/indent-blankline.nvim',
    config = function()
      require('indent_blankline').setup({
        filetype_exclude = {"startify"}
      })
    end
  }

  -- git support
  use 'tpope/vim-fugitive'
  use 'tpope/vim-rhubarb'
  use {
    'lewis6991/gitsigns.nvim',
    requires = {
      'nvim-lua/plenary.nvim'
    },
    config = function()
      require('gitsigns').setup()
    end
  }
  use {
    'ruifm/gitlinker.nvim',
    requires = 'nvim-lua/plenary.nvim',
    config = function()
      require('gitlinker').setup{}
    end
  }

  -- LSP
  use 'williamboman/mason.nvim'
  use 'williamboman/mason-lspconfig.nvim'
  use 'neovim/nvim-lspconfig'
  use {
    "folke/trouble.nvim",
    requires = "kyazdani42/nvim-web-devicons"
  }
  use 'j-hui/fidget.nvim'
  use {
    'jose-elias-alvarez/null-ls.nvim',
    requires = {{'nvim-lua/popup.nvim'}, {'nvim-lua/plenary.nvim'}}
  }

  -- autocompletion
  use {
    'hrsh7th/nvim-cmp',
    requires = {
      { 'hrsh7th/cmp-nvim-lsp' },
      { 'hrsh7th/cmp-buffer' },
      { 'saadparwaiz1/cmp_luasnip' }, -- snippet completions
      { 'onsails/lspkind-nvim' }
    }
  }

  -- rspec
  use {'keith/rspec.vim', opt = true}

  -- snippets
  use "L3MON4D3/LuaSnip" --snippet engine
  use "rafamadriz/friendly-snippets" -- a bunch of snippets to use

  -- treesitter
  use {'nvim-treesitter/nvim-treesitter', run = ':TSUpdate'}
  use {'nvim-treesitter/playground'}
  use {'RRethy/nvim-treesitter-endwise'}
  use {'windwp/nvim-ts-autotag'}

  -- autopairs
  use {'windwp/nvim-autopairs'}

  -- telescope
  use {
    'nvim-telescope/telescope.nvim',
    requires = {{'nvim-lua/popup.nvim'}, {'nvim-lua/plenary.nvim'}}
  }
  use 'nvim-telescope/telescope-project.nvim'

  -- themes
  use 'EdenEast/nightfox.nvim'
  -- use 'folke/tokyonight.nvim'

  -- undo
  use 'mbbill/undotree'

  -- projects
  use 'airblade/vim-rooter'

  -- orgmode
  use {
    'kristijanhusak/orgmode.nvim',
    config = function()
        require('orgmode').setup{}
    end
  }

  use 'nvim-telescope/telescope-symbols.nvim'

  -- bufferline and statusline
  use {
    "nanozuki/tabby.nvim",
    config = function()
      require("tabby").setup({
        tabline = require("tabby.presets").tab_with_top_win,
      })
    end
  }
  use {
    'nvim-lualine/lualine.nvim',
    requires = { 'kyazdani42/nvim-web-devicons', opt = true }
  }

  -- tests
  use 'klen/nvim-test'

  -- telekasten
  use {
    'renerocksai/telekasten.nvim',
    requires = {
      'renerocksai/calendar-vim'
    }
  }
  use {
    'iamcco/markdown-preview.nvim',
    run = function() vim.fn["mkdp#util#install"]() end,
  }
end)
