--
-- Map all keybindings here
--
local map = vim.api.nvim_set_keymap
local opts = { noremap = true, silent = true }

map("", "<Space>", "<Nop>", opts)
vim.g.mapleader = " "

map('n', '<F5>', ':UndotreeToggle<CR>', opts)
map('n', '<F8>', ':Neotree toggle reveal<CR>', opts)
map('n', '<F9>', ':Neotree source=git_status toggle<CR>', opts)
map('n', '<M-x>', ':Telescope commands theme=get_dropdown<CR>', opts)

-- Window navigation
map("n", "<C-h>", "<C-w>h", opts)
map("n", "<C-j>", "<C-w>j", opts)
map("n", "<C-k>", "<C-w>k", opts)
map("n", "<C-l>", "<C-w>l", opts)

-- Buffers navigation
map("n", "<S-l>", ":bnext<CR>", opts)
map("n", "<S-h>", ":bprevious<CR>", opts)

-- Resize with arrows
map("n", "<C-Up>", ":resize -2<CR>", opts)
map("n", "<C-Down>", ":resize +2<CR>", opts)
map("n", "<C-Left>", ":vertical resize -2<CR>", opts)
map("n", "<C-Right>", ":vertical resize +2<CR>", opts)

-- Move text up and down
map("n", "<A-j>", "<Esc>:m .+1<CR>==gi", opts)
map("n", "<A-k>", "<Esc>:m .-2<CR>==gi", opts)

-- Press jk fast to enter
map("i", "jk", "<ESC>", opts)

-- Indent text in visual mode
map("v", "<", "<gv", opts)
map("v", ">", ">gv", opts)

-- Move text up and down in visual mode
map("v", "<A-j>", ":m .+1<CR>==", opts)
map("v", "<A-k>", ":m .-2<CR>==", opts)
map("v", "p", '"_dP', opts)

-- Visual Block --
-- Move text up and down
map("x", "J", ":move '>+1<CR>gv-gv", opts)
map("x", "K", ":move '<-2<CR>gv-gv", opts)
map("x", "<A-j>", ":move '>+1<CR>gv-gv", opts)
map("x", "<A-k>", ":move '<-2<CR>gv-gv", opts)


map('n', '<leader>', ":WhichKey '<Space>'<CR>", opts)

local keymap = {
  g = {
    name = '+git',
    g = { '<cmd>Git<CR>', 'Git status' },
    b = { '<cmd>Git blame<CR>', 'Git blame' },
    l = { '<cmd>Git log<CR>', 'Git log' },
  },
  p = {
    name = '+project',
    f = {'<cmd>Telescope find_files<CR>', 'Find file'},
    p = {'<cmd>Telescope project<CR>', 'Find project'},
    s = {'<cmd>Telescope live_grep<CR>', 'Live grep'}
  },
  b = {
    name = '+buffers',
    a = {'<cmd>Telescope buffers sort_mru=true ignore_current_buffer=true<CR>', 'List all buffers'},
    b = {'<cmd>Telescope buffers sort_mru=true ignore_current_buffer=true only_cwd=true<CR>', 'List project buffers'}
  },
  r = { -- find a better key
    name = '+run tests',
    a = { '<cmd>TestSuite<cr>', 'Run the whole test suite' },
    e = { '<cmd>TestEdit<cr>', 'Edit tests for the current file' },
    f = { '<cmd>TestFile<cr>', 'Run all tests in the current file' },
    i = { '<cmd>TestInfo<cr>', 'Show information about the plugin' },
    l = { '<cmd>TestLast<cr>', 'Run the last test' },
    r = { '<cmd>TestNearest<cr>', 'Run the test nearest to the cursor' },
    v = { '<cmd>TestVisit<cr>', 'Open the last ran test in the current buffer' },
  },
  t = {
    name = '+term',
    t = {'<cmd>1ToggleTerm size=30<CR>', 'Toggle term'},
    T = {'<cmd>2ToggleTerm size=30 direction=float<CR>', 'Toggle floating term'},
    h = {'<cmd>lua require"user.term".terminals.htop:toggle()<CR>', 'Open htop on a term'}
  },
  j = {
    name = '+jump',
    j = {"<cmd>lua require'hop'.hint_words()<cr>"},
    l = {"<cmd>lua require'hop'.hint_lines()<cr>"}
  },
  f = {
    name = '+find',
    D = {'<cmd>lua vim.lsp.buf.declaration()<CR>', 'Find declaration'},
    d = {'<cmd>lua vim.lsp.buf.definition()<CR>', 'Find definition'},
    i = {'<cmd>lua vim.lsp.buf.implementation()<CR>', 'Find implementation'},
    m = {'<cmd>Telescope manpages theme=get_dropdown<CR>', 'Find manpage'},
    r = {'<cmd>Telescope oldfiles theme=get_dropdown<CR>', 'Find recent file'},
    s = {'<cmd>lua require"telescope.builtin".symbols({ sources = {"emoji", "gitmoji"} })<cr>', 'Find symbol'}
  },
  l = {
    name = '+LSP',
    r = {'<cmd>lua vim.lsp.buf.rename()<CR>', 'Rename'},
    a = {'<cmd>lua vim.lsp.buf.code_action()<CR>', 'Code actions'},
    j = {'<cmd>lua vim.lsp.diagnostic.goto_prev()<CR>', "Prev diagnostic"},
    k = {'<cmd>lua vim.lsp.diagnostic.goto_next()<CR>', "Next diagnostic"},
    q = {'<cmd>lua vim.lsp.diagnostic.set_loclist()<CR>', 'Quickfix'},
    f = {'<cmd>lua vim.lsp.buf.formatting()<CR>', 'Format'},
    d = {'<cmd>lua vim.diagnostic.open_float({type="buffer"})<CR>', 'Show diagnostics'},
    S = {'<cmd>Telescope lsp_dynamic_workspace_symbols theme=get_dropdown<CR>', 'Find workspace symbol'},
    s = {'<cmd>Telescope lsp_document_symbols theme=get_dropdown<CR>', 'Find document symbol'},
    w = {
      name = '+workspaces',
      a = {'<cmd>lua vim.lsp.buf.add_workspace_folder()<CR>', 'Add folder to workspace'},
      r = {'<cmd>lua vim.lsp.buf.remvoe_workspace_folder()<CR>', 'Remove folder from workspace'},
      l = {'<cmd>lua print(vim.inspect(vim.lsp.buf.list_workspace_folders()))<CR>', 'List workspace folders'},
    },
  },
  x = {
    name = '+Trouble',
    x = {'<cmd>TroubleToggle<CR>', 'Toggle diagnostics split'},
    w = {'<cmd>TroubleToggle workspace_diagnostics<CR>', 'Toggle workspace diagnostics split'},
    d = {'<cmd>TroubleToggle document_diagnostics<CR>', 'Toggle documents diagnostics split'},
    q = {'<cmd>TroubleToggle quickfix<CR>', 'Toggle diagnostics quickfix'},
    l = {'<cmd>TroubleToggle loclist<CR>', 'Toggle diagnostics loclist'}
  },
  z = {
    name = '+Zettelkasten',
    f = {"<cmd>lua require('telekasten').find_notes()<CR>", "Find notes"},
    d = {"<cmd>lua require('telekasten').find_daily_notes()<CR>", "Find daily notes"},
    g = {"<cmd>lua require('telekasten').search_notes()<CR>", "Search notes"},
    l = {"<cmd>lua require('telekasten').follow_link()<CR>", "Follow link"},
    z = {"<cmd>lua require('telekasten').panel()<CR>", "Panel"}
  }
}

local wk = require('whichkey_setup')
wk.config{
  hide_statusline = false,
  default_keymap_settings = {
    silent = true,
    noremap = true
  },
  default_mode = 'n'
}
wk.register_keymap('leader', keymap)

