-- init.lua

require('orgmode').setup_ts_grammar()

require('orgmode').setup({
  org_agenda_files = {'~/Nextcloud/org/*'},
  org_default_notes_file = '~/Nextcloud/org/refile.org',
  org_todo_keywords = {'TODO', 'STRT', 'WAIT', '|', 'DONE', 'CANCEL', 'HOLD'},
  org_indent_mode = 'noindent',
  org_hide_leading_stars = true,
  org_log_done = true
})
