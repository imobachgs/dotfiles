local status_ok, neo_tree = pcall(require, "neo-tree")
if not status_ok then
  return
end

neo_tree.setup({
  filesystem = {
    filtered_items = {
      visible = true,
    },
    hijack_netrw_behavior = "open_default"
  },
});
