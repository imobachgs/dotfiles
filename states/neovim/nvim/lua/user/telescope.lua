local status_ok, telescope = pcall(require, 'telescope')
if not status_ok then
  return
end

local theme = "dropdown"
local utils = require("utils");

local projects_dirs = os.getenv("PROJECTS")
local base_dirs;
if (projects_dirs == nil) then
  base_dirs = {}
else
  base_dirs = utils.split(os.getenv("PROJECTS"), ":")
end

telescope.setup{
  defaults = {
    path_display = { 'absolute' },
    winblend = 20,
    mappings = {
      i = {
        ['<C-h>'] = 'which_key'
      }
    }
  },
  pickers = {
    find_files = {
      theme = theme
    },
    live_grep = {
      theme = theme
    },
    buffers = {
      theme = theme
    }
  },
  extensions = {
    project = {
      base_dirs = base_dirs,
      theme = theme,
      order_by = 'recent'
    }
  }
}

telescope.load_extension('project')
