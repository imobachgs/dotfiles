local status_ok, sessions = pcall(require, "mini.sessions")
if not status_ok then
  return
end

-- sessions set up
sessions.setup{}

-- starter set up
local starter = require("mini.starter")

local telescope_actions = function()
  return function()
    return {
      { action = "Telescope project display_type=full", name = 'Project', section = 'Find' },
      { action = "Telescope find_files", name = 'File', section = 'Find' },
      { action = "Telescope live_grep", name = 'Live grep', section = 'Find' }
    }
  end
end

local bookmarks = function()
  return function()
    return {
      { action = ":e ~/.config/nvim", name = 'Neovim configuration', section = 'Bookmarks' }
    }
  end
end

starter.setup {
  items = {
    telescope_actions(),
    starter.sections.recent_files(),
    bookmarks(),
    starter.sections.builtin_actions()
  }
}
