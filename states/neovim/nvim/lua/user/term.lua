vim.api.nvim_set_keymap('t', '<Esc>', '<C-\\><C-n>', {})

local status_ok, toggleterm = pcall(require, "toggleterm");
if not status_ok then
  return
end

toggleterm.setup{}

local Terminal = require('toggleterm.terminal').Terminal;
local terminals = {}
terminals.htop = Terminal:new({ cmd = "htop", direction = "float", hidden = true });

M = {}
M.terminals = terminals;
return M;
