--
-- Treesitter
--
local parser_config = require 'nvim-treesitter.parsers'.get_parser_configs()
parser_config.ruby.filetype_to_parsername = 'rspec'

--
-- RSpec
--
vim.cmd("packadd rspec.vim")
local utils = require('utils')
utils.create_augroup({
  {"BufNewFile,BufRead", "*_test.rb", "set ft=rspec syntax=rspec"},
}, "rspec_autocmds")
