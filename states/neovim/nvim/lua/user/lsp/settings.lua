M = {};

M.solargraph = {
  autoformat = false,
  diagnostics = false,
}

M.sumneko_lua = {
  Lua = {
    diagnostics = {
      globals = { "vim" }
    }
  }
}

local json_schemas = {
  {
    description = "NPM configuration file",
    fileMatch = {
      "package.json",
    },
    url = "https://json.schemastore.org/package.json",
  }, {
    description = "ESLint config",
    fileMatch = {
      ".eslintrc.json",
      ".eslintrc",
    },
    url = "https://json.schemastore.org/eslintrc.json",
  }, {
    description = "Prettier config",
    fileMatch = {
      ".prettierrc",
      ".prettierrc.json",
      "prettier.config.json",
    },
    url = "https://json.schemastore.org/prettierrc",
  }
}

M.jsonls = {
  json = {
    schemas = json_schemas,
  }
}

return M;
