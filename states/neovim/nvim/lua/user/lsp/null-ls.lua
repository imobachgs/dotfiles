local null_ls_status_ok, null_ls = pcall(require, "null-ls")
if not null_ls_status_ok then
  return
end

local formatting = require("null-ls").builtins.formatting
local diagnostics = require("null-ls").builtins.diagnostics

null_ls.setup({
  debug = true,
	sources = {
		diagnostics.eslint,
		diagnostics.rubocop,
		formatting.eslint,
		formatting.rubocop,
		formatting.rustfmt,
		formatting.stylua,
    formatting.prettier,
	},
})
