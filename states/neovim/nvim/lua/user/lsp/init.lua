local status_ok, mason = pcall(require, "mason")
if not status_ok then
  return
end

local on_attach = function(client, bufnr)
  local function buf_set_keymap(...) vim.api.nvim_buf_set_keymap(bufnr, ...) end
  -- local function buf_set_option(...) vim.api.nvim_buf_set_option(bufnr, ...) end

  -- -- Enable completion triggered by <c-x><c-o>
  -- buf_set_option('omnifunc', 'v:lua.vim.lsp.omnifunc')

  -- Mappings.
  local opts = { noremap=true, silent=true }

  -- See `:help vim.lsp.*` for documentation on any of the below functions
  buf_set_keymap('n', 'gD', '<cmd>lua vim.lsp.buf.declaration()<CR>', opts)
  buf_set_keymap('n', 'gd', '<cmd>lua vim.lsp.buf.definition()<CR>', opts)
  buf_set_keymap('n', 'K', '<cmd>lua vim.lsp.buf.hover()<CR>', opts)
  buf_set_keymap('n', 'gi', '<cmd>lua vim.lsp.buf.implementation()<CR>', opts)
  buf_set_keymap('n', '<C-s>', '<cmd>lua vim.lsp.buf.signature_help()<CR>', opts)
  buf_set_keymap('n', '<space>D', '<cmd>lua vim.lsp.buf.type_definition()<CR>', opts)
  buf_set_keymap('n', 'gr', '<cmd>lua vim.lsp.buf.references()<CR>', opts)
end

local capabilities = require('cmp_nvim_lsp').default_capabilities()
local lsp_flags = { debounce_text_changes = 150 } -- to be the default in neovim 0.7+

-- Servers configuration
local settings = require('user.lsp.settings')

-- List of servers to install and set up
local servers = {
  "eslint",
  "jsonls",
  "rust_analyzer",
  "solargraph",
  "sumneko_lua",
  "tsserver"
}

mason.setup()
local mason_lspconfig = require("mason-lspconfig")
mason_lspconfig.setup{
  ensure_installed = servers,
  automatic_installation = true
}

local lspconfig = require("lspconfig")
for _, name in pairs(mason_lspconfig.get_installed_servers()) do
  local opts = {
    on_attach = on_attach,
    capabilities = capabilities,
    flags = lsp_flags
  }

  if settings[name] then
    local server_opts = settings[name]
    opts = vim.tbl_deep_extend("force", opts, {
      settings = server_opts
    });
  end

  lspconfig[name].setup(opts)
end

vim.diagnostic.config({signs = false})

local fidget_ok, fidget = pcall(require, "fidget")
if fidget_ok then
  fidget.setup{}
end

require("user.lsp.null-ls")
