local colorscheme = "nightfox" -- nightfox, nordfox, dayfox, dawnfox, duskfox and terafox

require("lualine").setup({
  options = {
    theme = colorscheme
  }
})

vim.cmd("colorscheme " .. colorscheme)
