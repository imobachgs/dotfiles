local status_ok, startup = pcall(require, "alpha")
if not status_ok then
	return
end

local theme = require("alpha.themes.theta")
startup.setup(theme.config)
