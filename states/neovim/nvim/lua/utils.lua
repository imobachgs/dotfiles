-- https://icyphox.sh/blog/nvim-lua/
local M = {}
local cmd = vim.cmd

function M.create_augroup(autocmds, name)
    cmd('augroup ' .. name)
    cmd('autocmd!')
    for _, autocmd in ipairs(autocmds) do
        cmd('autocmd ' .. table.concat(autocmd, ' '))
    end
    cmd('augroup END')
end

function M.split(input, sep)
  local values = {}
  for str in string.gmatch(input, "([^"..sep.."]+)") do
    table.insert(values, str)
  end
  return values
end

return M
