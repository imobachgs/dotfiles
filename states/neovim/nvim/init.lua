-- This configuration is partially inspired by
-- https://github.com/LunarVim/Neovim-from-scratch/

require 'user.plugins'
require 'user.lang.ruby' 
require 'user.term'
require('templates')
require 'user.options'
require 'user.keymap'
require 'user.colorscheme'
require 'user.cmp'
require 'user.lsp'
-- require 'user.dap'
require 'user.org'
require 'user.telekasten'
require 'user.treesitter'
require 'user.autopairs'
require 'user.telescope'
require 'user.filetree'
require 'user.sessions'
require 'user.test'
