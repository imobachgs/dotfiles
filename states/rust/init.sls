rust_packages:
  pkg.installed:
    - pkgs:
      - rustup
      - lldb

install_toolchain:
  cmd.run:
    - name: rustup toolchain install stable
    - runas: {{ grains.user }}
    - require:
      - pkg: rust_packages

# curl -L https://github.com/rust-lang/rust-analyzer/releases/download/2022-08-01/rust-analyzer-x86_64-unknown-linux-gnu.gz | gunzip -c > rust-analyzer
