# dotfiles

This repository contains a set of Salt states aimed to set up my development environment, based on
[openSUSE Tumbleweed][tw]. Bear in mind that the content of this repository is tailored to my use
case. Anyway, do not hesitate to take anything you find useful for you.

## How to use it

Before running `make`, copy the configuration file example and adjust it to your taste:

        $ cp pillar/config.example.sls pillar/config.sls

Now you can use `make`, which will install (if missing) and run `salt`:

        $ make

Just grab a coffe while Salt does its magic.

## States

* `console`: install and set up console related tools, shells, etc.
* `desktop`: install desktop applications, fonts, etc.
* `javascript`: install JavaScript related tools.
* `neovim`: install and set up neovim. **It is still a work in progress**.
* `osc`: install and set up `osc` ([openSUSE Build Service][obs] client)
* `ruby`: install Ruby packages, including development ones.
* `rust`: install Rust related tools using [rustup].

## Acknowledgments

I borrowed the idea of using a `Makefile` to prepare the grains and run Salt from
[rawkode/dotfiles][rawkode].

[neovim]: https://neovim.io/
[obs]: https://build.opensuse.org/
[rawkode]: https://github.com/rawkode/dotfiles/
[ruby]: https://ruby-lang.org/
[rustup]: https://rustup.rs/
[salt]: https://saltproject.io/
[tw]: https://get.opensuse.org/tumbleweed/
